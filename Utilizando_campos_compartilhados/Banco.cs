﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Utilizando_campos_compartilhados
{
    class Banco
    {
        public static int soma;

        public Banco()
        {
            soma = 20;
        }
        public void incrementa()
        {
            soma++;
        }
        public void incrementa(int valor)
        {
            soma += valor;
        }
    }
}
