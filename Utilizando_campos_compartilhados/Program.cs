﻿using System;

namespace Utilizando_campos_compartilhados
{
    class Program
    {
        static void Main(string[] args)
        {
            Banco objeto01 = new Banco();
            Banco objeto02 = new Banco();

            objeto01.incrementa();
            Console.WriteLine("O valor é {0}", Banco.soma.ToString());
            Console.WriteLine("Digite um valor: ");

            objeto02.incrementa(int.Parse(Console.ReadLine()));
            Console.WriteLine("O valor atualizado é {0}", Banco.soma.ToString());
        }
        
    }
}
